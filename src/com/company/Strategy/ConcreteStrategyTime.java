package com.company.Strategy;

import com.company.Consumer.Server;
import com.company.Consumer.Task;

import java.util.Iterator;
import java.util.List;
import java.util.concurrent.LinkedBlockingQueue;

public class ConcreteStrategyTime implements Strategy {

    @Override
    public void addTask(List<Server> servers, Task t) {

        int min=0,ct=0,chosenServer=0;
        //find closed queues
        for(int i=0;i<servers.size();i++){
            if(servers.get(i).getTasks().peek()==null)
            {
                servers.get(i).addTask(t);
                return;
            }
        }
        //insert in queue with lowest time;
        for(int i=0;i<servers.size();i++){
            for(int j=0;j<servers.get(i).getTasks().size();j++){
                LinkedBlockingQueue tasks=servers.get(i).getTasks();
                Iterator<Task> ts=tasks.iterator();
                while(ts.hasNext()){
                    Task taskul=ts.next();
                    ct=taskul.getProccessingTime();
                }
                if(ct<min){
                    min=ct;
                    ct=0;
                    chosenServer=i;
                }
            }
        }
    servers.get(chosenServer).addTask(t);
}
    }
