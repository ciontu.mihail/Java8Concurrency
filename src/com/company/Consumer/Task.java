package com.company.Consumer;

public class Task {
    private int ID;
    private int arrivalTime;
    private int proccessingTime;
    public Task(int ID,int arrivalTime,int proccessingTime){
        this.ID=ID;
        this.arrivalTime=arrivalTime;
        this.proccessingTime=proccessingTime;
    }
    public int getID(){
        return ID;
    }
    public int getProccessingTime() {
        return proccessingTime;
    }
    public int getArrivalTime(){
        return arrivalTime;
    }
    public void setProcessingTime(int time){
        this.proccessingTime=time;
    }
}
